# AWESOME

## [Docker](https://gitlab.com/Drake.aren/awesome/blob/master/docker_collection.md)

## UI prototyping

[precursorapp](https://precursorapp.com/)

## Workflow

### Group chat

[Slack](https://www.slack.com/)

### Task manager

[Trello](https://trello.com) 

## Git

[Sublime Merge](https://www.sublimemerge.com)

[GTM](https://github.com/git-time-metric/gtm) - Git time metric with git integration

## wallpaper

[wallpaperscraft](https://wallpaperscraft.com/)
 
[pexels](https://www.pexels.com/search/HD%20wallpaper/)

## Text-To-speech

[lyrebird](https://lyrebird.ai/vocal-avatar-api) - open vocal avatar

## Email service

[Protonmail](https://protonmail.com) - best mail service in 2k18

## Dotfiles

[yadm](https://thelocehiliosan.github.io/yadm/)

## Package manager 

[Linuxbrew](https://linuxbrew.sh/) - Brew port to linux

## PHP

### Debug

[xdebug](https://xdebug.org/) - php debuger

[psysh](https://github.com/bobthecow/psysh) - PsySH is a runtime developer console, interactive debugger and REPL for PHP

[wp-cli](https://github.com/wp-cli/shell-command) - Opens an interactive PHP console for running and testing PHP code.