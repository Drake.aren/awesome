# To read

[Awesome master](https://github.com/sindresorhus/awesome)

[Habrahabr](https://habr.com/top)

# Github

[Hack with GitHub](https://github.com/Hack-with-Github)

[Awesome hacking](https://github.com/carpedm20/awesome-hacking)

[Awesome Hacking Resources](https://github.com/vitalysim/Awesome-Hacking-Resources)

[Free Security eBooks](https://github.com/Hack-with-Github/Free-Security-eBooks)

[Hacking Tools](https://github.com/m4ll0k/Awesome-Hacking-Tools)

## Tool pack

[Mr.Robot](https://github.com/Manisso/fsociety)

## 中国

[learn-hacking](https://github.com/tiancode/learn-hacking)

[超棒黑客必备表单](https://github.com/sunnyelf/awesome-hacking)